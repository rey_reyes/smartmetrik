(function(){
    'use strict';

    angular
        .module('smartmetrik')
        .factory('Canvas', Canvas)

    Canvas.$inject = ['$resource','$rootScope'];
    function Canvas($resource, $rootScope){
        return $resource('http://localhost:8000/app/data/dataExample.json');
    };
})();
