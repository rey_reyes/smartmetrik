(function(){
    'use strict';

    angular
        .module('smartmetrik')
        .controller('CanvasCtrl', CanvasCtrl);

    CanvasCtrl.$inject = ['$modal','$scope', '$state','Canvas'];
    function CanvasCtrl ($modal,$scope,$state,Canvas){
        var vm = this;
        Canvas.query({},
            function(success){
            vm.objectList = success;
        });

        vm.profile = function (){
            $state.go('editUser');
        };

        vm.message1 = function (objectList)
        {
            vm.title = 'Posición';
            vm.message = 'Estás ubicado en la posición #: '+objectList.id;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/listGeneric/partials/modal/message_modal.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard  : false
            });
        }

        vm.message2 = function (objectList)
        {
            vm.title = 'Posición';
            vm.message = 'La información completa es: '+objectList.id+ ' '+objectList.title + ' '+objectList.description;
            $scope.modalInstance = $modal.open({
                templateUrl: 'modules/listGeneric/partials/modal/message_modal.html',
                scope: $scope,
                size: 'sm',
                backdrop: 'static',
                keyboard  : false
            });
        }

        vm.ok = function()
        {
            $scope.modalInstance.dismiss('cancel');
        }
    };
})();