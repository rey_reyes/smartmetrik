(function (){
    'user strict';
    
    angular.module('smartmetrik', ['ngResource','ui.bootstrap','ui.router'])
    .config( function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
      	.state('index', {
        	abstract: true,
        	views: {
         		'navbar': {
                	templateUrl: 'templates/navbar.html'
            	},
            }
        })
        .state('home', {
        	parent:'index',
            url: '/',
            views: {
				'canvas@': {
					templateUrl:'templates/canvas.html',
					controller: 'CanvasCtrl',
                    controllerAs: 'vm'
				}
        	}
    	})      
    })
})();
